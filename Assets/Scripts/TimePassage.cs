﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePassage : MonoBehaviour
{
    [SerializeField]
    private GameObject light = null;

    [SerializeField]
    private float timeScale = 1.5f;

    [SerializeField]
    private float length = 10.0f;
    private float dayTtl;

    private Vector3 beginRot;
    private Vector3 endRot;

    bool isDay = true;

    [SerializeField]
    GameObject nextDayPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        beginRot = new Vector3(0, -90, 0);
        endRot = new Vector3(180, -90, 0);
        timeScale = 220 / length;
        Init();
        World.instance.OnNewDay += Init;
    }

    private void Init()
    {
        dayTtl = length;

        light.transform.rotation = Quaternion.Euler(beginRot);
    }

    // Update is called once per frame
    void Update()
    {
        if(isDay && World.instance.IsLevelRunning())
        {
            light.transform.Rotate(Vector3.right, Time.deltaTime * timeScale);
            dayTtl -= Time.deltaTime;
            if(dayTtl <= 0)
            {
                Debug.Log("Day complete.");
                nextDayPanel.SetActive(true);
                World.instance.UpdateMelonCount();
                World.instance.PauseTime();
            }
        }
    }

    public float GetTimeRemaining()
    {
        return dayTtl;
    }
}
