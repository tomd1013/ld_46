﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioPlay : MonoBehaviour
{

    [SerializeField]
    private AudioClip[] clips = null;
    private AudioSource audioSource = null;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayRandom()
    {
        int i = Random.Range(0, clips.Length);
        audioSource.clip = clips[i];
        audioSource.Play();
    }
}
