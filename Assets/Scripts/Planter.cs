﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planter : MonoBehaviour, IDamageable
{
    bool isWatered = false;
    const float waterTtl = 15;
    float curWaterTtl = 0;

    const float driedTtl = 15;
    float curDriedTtl = 0;

    [SerializeField]
    private GameObject plant = null;

    public delegate void PlantEvent();
    public PlantEvent onPlantDeath;
    public PlantEvent onSwatted;
    public PlantEvent onWatered;

    [SerializeField]
    private List<MeshRenderer> plantBody = null;
    [SerializeField]
    private Color healthyPlantColor = Color.white;

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
        World.instance.OnNewDay += Init;
    }

    // Update is called once per frame
    void Update()
    {
        if(World.instance.IsLevelRunning())
        {
            if (isWatered)
            {
                curWaterTtl -= Time.deltaTime;
                if (curWaterTtl <= 0)
                {
                    isWatered = false;
                    curWaterTtl = 0;
                    curDriedTtl = driedTtl;
                    foreach(MeshRenderer r in plantBody)
                    {
                        r.material.color = Color.white;
                    }
                }
            }
            else
            {
                curDriedTtl -= Time.deltaTime;
                if (curDriedTtl <= 0)
                {
                    OnKilled();
                }
            }
        }
    }

    private void Init()
    {
        gameObject.SetActive(true);
        isWatered = true;
        curWaterTtl = waterTtl;
        curDriedTtl = driedTtl;
        foreach (MeshRenderer r in plantBody)
        {
            r.material.color = healthyPlantColor;
        }
    }

    private void OnKilled()
    {
        Debug.Log("Plant Died! - " + name);
        onPlantDeath?.Invoke();
        gameObject.SetActive(false);
    }

    public void WaterPlant()
    {
        isWatered = true;
        curWaterTtl = waterTtl;
        foreach (MeshRenderer r in plantBody)
        {
            r.material.color = healthyPlantColor;
        }
        onWatered?.Invoke();
    }

    public void ScatterBirds()
    {
        onSwatted?.Invoke();
    }

    public void Damage(float amt)
    {
        if(isWatered)
        {
            curWaterTtl -= amt;
        } else
        {
            curDriedTtl -= amt;
        }
    }
}
