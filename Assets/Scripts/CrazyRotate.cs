﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazyRotate : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotateVec = Vector3.one;
    [SerializeField]
    private float timeScale = .5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotateVec, Time.deltaTime * timeScale);
    }
}
