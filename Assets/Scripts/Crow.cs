﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crow : MonoBehaviour
{

    private Planter planter_1 = null;
    private Planter planter_2 = null;
    private Planter targetPlanter = null;

    private Vector3 dest = Vector3.zero;

    private float xOffsetRange = 0.2f;
    private float zOffsetRange = 0.5f;

    private float minDmg = 2.0f;
    private float maxDmg = 15.0f;
    private float damageAmt;

    private float minPeckDuration = 2;
    private float maxPeckDuration = 6;
    private float curPeckTtl;

    private AudioSource audioSrc = null;

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        curPeckTtl = 5;
        Init();

        
    }

    // Update is called once per frame
    void Update()
    {
        if(World.instance.IsLevelRunning())
        {
            if (dest == Vector3.zero)
            {
                Destroy(gameObject);
                return;
            }

            curPeckTtl -= Time.deltaTime;

            if (curPeckTtl <= 0)
            {
                int playSound = Random.Range(0, 10);
                if (playSound > 3)
                {
                    audioSrc.Play();
                }
                targetPlanter.Damage(damageAmt);
                curPeckTtl = Random.Range(minPeckDuration, maxPeckDuration);
            }

            transform.position = Vector3.Lerp(transform.position, dest, 1 * Time.deltaTime);
        }
    }

    private void Init()
    {
        planter_1 = World.instance.GetPlanter1();
        planter_2 = World.instance.GetPlanter2();

        int target = Random.Range(0, 2);
        if (target == 0)
        {
            if (planter_1.isActiveAndEnabled)
            {
                targetPlanter = planter_1;
            }
            else if (!planter_1.isActiveAndEnabled && planter_2.isActiveAndEnabled)
            {
                targetPlanter = planter_2;
            }
            else
            {
                targetPlanter = null;
            }
        }
        else
        {
            if (planter_2.isActiveAndEnabled)
            {
                targetPlanter = planter_2;
            }
            else if (!planter_2.isActiveAndEnabled && planter_1.isActiveAndEnabled)
            {
                targetPlanter = planter_1;
            }
            else
            {
                targetPlanter = null;
            }
        }

        if (targetPlanter != null && targetPlanter.isActiveAndEnabled)
        {
            damageAmt = Random.Range(minDmg, maxDmg);
            curPeckTtl = Random.Range(minPeckDuration, maxPeckDuration);

            targetPlanter.onPlantDeath += DestroySelf;
            targetPlanter.onSwatted += Swatted;
            World.instance.OnNewDay += DestroySelf;
            float xOffset = Random.Range(-xOffsetRange, xOffsetRange);
            float zOffset = Random.Range(-zOffsetRange, zOffsetRange);
            dest = new Vector3(targetPlanter.transform.position.x + xOffset,
                targetPlanter.transform.position.y,
                targetPlanter.transform.position.z + zOffset);
        }
    }

    private void Swatted()
    {
        // Increment birds swatted world counter
        World.instance.IncrementCrowCount();
        DestroySelf();
    }

    private void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        Debug.Log("Bird dead: " + name);
        if(targetPlanter != null)
        {
            targetPlanter.onPlantDeath -= DestroySelf;
            targetPlanter.onSwatted -= Swatted;
            World.instance.OnNewDay -= DestroySelf;
        }
    }
}
