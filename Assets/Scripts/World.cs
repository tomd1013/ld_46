﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public static World instance;

    [SerializeField]
    private Planter p1 = null;
    [SerializeField]
    private Planter p2 = null;

    private bool isLevelRunning = true;
    private int crowsKilled = 0;
    private int day = 1;

    private int totalCrows = 0;
    private int totalMelons = 0;

    public delegate void WorldEvent();
    public WorldEvent OnNewDay;

    [SerializeField]
    GameObject endGamePanel = null;

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        OnNewDay?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        if(isLevelRunning)
        {
            if(!p1.isActiveAndEnabled && !p2.isActiveAndEnabled)
            {
                Debug.Log("Defeated. Crows Swatted: " + crowsKilled);
                isLevelRunning = false;
                endGamePanel.SetActive(true);
            }
        }
    }

    public Planter GetPlanter1()
    {
        return p1;
    }

    public Planter GetPlanter2()
    {
        return p2;
    }

    public void IncrementCrowCount()
    {
        crowsKilled++;
        totalCrows++;
    }

    public void UpdateMelonCount()
    {
        if (p1.isActiveAndEnabled)
        {
            totalMelons++;
        }
        if (p2.isActiveAndEnabled)
        {
            totalMelons++;
        }
    }

    public void IncrementDay()
    {
        day++;
        
        isLevelRunning = true;
        OnNewDay?.Invoke();
    }

    public void PauseTime()
    {
        isLevelRunning = false;
    }

    public bool IsLevelRunning()
    {
        return isLevelRunning;
    }

    public int GetCrowCount()
    {
        return crowsKilled;
    }

    public int GetDay()
    {
        return day;
    }

    public int GetScore()
    {
        return (totalMelons+1) * totalCrows;
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
}
