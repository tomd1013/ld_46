﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{

    [SerializeField]
    private AudioClip plantDeathSound = null;
    private AudioSource src = null;

    [SerializeField]
    private Planter planter = null;

    [SerializeField]
    private GameObject melon = null;
    private bool isDead = false;
    [SerializeField]
    private float scaleMult = 0.02f;


    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
        src.clip = plantDeathSound;
        planter.onPlantDeath += PlaySound;
        planter.onPlantDeath += OnDeath;
        World.instance.OnNewDay += Init;
        Init();
    }

    private void Update()
    {
        if(!isDead && World.instance.IsLevelRunning())
        {
            float amt = Time.deltaTime * scaleMult;
            melon.transform.localScale += new Vector3(amt, amt, amt*1.25f);
        }
    }

    private void Init()
    {
        isDead = false;
        melon.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    private void OnDeath()
    {
        isDead = true;
        melon.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        // Queue explosion
    }

    private void PlaySound()
    {
        src.Play();
    }
}
