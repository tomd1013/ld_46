﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSoundPlayer : MonoBehaviour
{

    [SerializeField]
    private Planter planter = null;
    [SerializeField]
    private AudioClip clip = null;

    private AudioSource src = null;
    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
        src.clip = clip;
        planter.onWatered += PlaySound;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void PlaySound()
    {
        src.Play();
    }
}
