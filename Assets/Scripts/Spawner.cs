﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject spawnable = null;

    [SerializeField]
    private int maxSpawnCount = 3;

    [SerializeField]
    private float minSpawnDuration = 3;
    [SerializeField]
    private float maxSpawnDuration = 6;

    private float curSpawnTtl = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Spawn();
        Init();
        World.instance.OnNewDay += Init;
    }

    // Update is called once per frame
    void Update()
    {
        if (World.instance.IsLevelRunning())
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                Spawn();
            }

            curSpawnTtl -= Time.deltaTime;
            if (curSpawnTtl <= 0)
            {
                Spawn();
            }
        }
    }

    public void Init()
    {
        curSpawnTtl = Random.Range(5, 12);
        maxSpawnCount += 1;
    }


    private void Spawn()
    {
        int count = Random.Range(1, maxSpawnCount + 1);
        for (int i = 0; i < count; i++)
        {
            GameObject.Instantiate(spawnable, transform.position, transform.rotation, null);
        }

        curSpawnTtl = Random.Range(minSpawnDuration, maxSpawnDuration);
    }

    public void SetMaxSpawnCount(int max)
    {
        maxSpawnCount = max;
    }

    public void SetSpawnDuration(float min, float max)
    {
        minSpawnDuration = min;
        maxSpawnDuration = max;
    }
}
