﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerSoundPlayer : MonoBehaviour
{
    [SerializeField]
    private Planter planter = null;
    [SerializeField]
    private AudioClip clip = null;

    private AudioSource src = null;
    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
        src.clip = clip;
        planter.onSwatted += PlaySound;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void PlaySound()
    {
        src.Play();
    }
}
