﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Text crowsKilled = null;
    [SerializeField]
    private Image plant1Destroyed = null;
    [SerializeField]
    private Image plant2Destroyed = null;
    [SerializeField]
    private Text timeRemaining = null;
    [SerializeField]
    private Text day = null;

    [SerializeField]
    private TimePassage timePassage = null;

    [SerializeField]
    private Text currScore = null;
    [SerializeField]
    private Text totalScore = null;

    // Start is called before the first frame update
    void Start()
    {
        World.instance.OnNewDay += Init;
    }

    private void Init()
    {
        plant1Destroyed.enabled = false;
        plant2Destroyed.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        crowsKilled.text = World.instance.GetCrowCount().ToString();
        if(!World.instance.GetPlanter1().isActiveAndEnabled)
        {
            plant1Destroyed.enabled = true;
        }

        if (!World.instance.GetPlanter2().isActiveAndEnabled)
        {
            plant2Destroyed.enabled = true;
        }

        timeRemaining.text = timePassage.GetTimeRemaining().ToString("0.00");
        day.text = World.instance.GetDay().ToString();

        currScore.text = World.instance.GetScore().ToString();
        totalScore.text = World.instance.GetScore().ToString();
    }
}
