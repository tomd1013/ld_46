﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteraction : MonoBehaviour
{
    private Planter interactablePlanter = null;
    private Planter altPlanter = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            interactablePlanter?.WaterPlant();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(null == interactablePlanter)
        {
            Planter p = other.GetComponentInChildren<Planter>();
            if(null != p)
            {
                interactablePlanter = p;
                Debug.Log("Trigger Enter: " + p.gameObject.name);
            }
        }
        else
        {
            Planter p = other.GetComponentInChildren<Planter>();
            if(null != p)
            {
                altPlanter = p;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(null != interactablePlanter)
        {
            Planter p = other.GetComponentInChildren<Planter>();
            if(null != p && interactablePlanter == p)
            {
                if(null != altPlanter)
                {
                    interactablePlanter = altPlanter;
                    altPlanter = null;
                }
                else
                {
                    interactablePlanter = null;
                }
                Debug.Log("Trigger Exit: " + p.gameObject.name);
            }
        }
    }
}
