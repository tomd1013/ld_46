﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum Looking
{
    CENTER,
    LEAN_LEFT,
    LEAN_RIGHT,
    LOOK_LEFT,
    LOOK_RIGHT
}

public class SeatedCharacterInteraction : MonoBehaviour
{
    public UnityEvent OnMovement;

    [SerializeField]
    private Camera characterCam = null;

    private Looking looking;

    [SerializeField]
    private Transform leanLeft = null;
    [SerializeField]
    private Transform leanRight = null;
    [SerializeField]
    private Transform lookLeft = null;
    [SerializeField]
    private Transform lookRight = null;
    [SerializeField]
    private Transform center = null;

    [SerializeField]
    private float mvmtMult = 2;

    // Start is called before the first frame update
    void Start()
    {
        looking = Looking.CENTER;
    }

    // Update is called once per frame
    void Update()
    {
        if(World.instance.IsLevelRunning())
        {
            HandleInput();
            HandleMouseInput();
        }
        

        UpdatePosition();
    }

    private void HandleInput()
    {
        if (looking == Looking.CENTER)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                // Lean left
                looking = Looking.LEAN_LEFT;
                OnMovement?.Invoke();
            }
            else if (Input.GetKeyDown(KeyCode.E))
            {
                // Lean right
                looking = Looking.LEAN_RIGHT;
                OnMovement?.Invoke();

            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                // Look left
                looking = Looking.LOOK_LEFT;
                OnMovement?.Invoke();
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                // Look right
                looking = Looking.LOOK_RIGHT;
                OnMovement?.Invoke();
            }
        }

        if (looking != Looking.CENTER)
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                // Look center
                looking = Looking.CENTER;
            }
            else if (Input.GetKeyUp(KeyCode.E))
            {
                // Look center
                looking = Looking.CENTER;
            }
            else if (Input.GetKeyUp(KeyCode.A))
            {
                // Look center
                looking = Looking.CENTER;
            }
            else if (Input.GetKeyUp(KeyCode.D))
            {
                // Look center
                looking = Looking.CENTER;
            }
        }
    }

    private void HandleMouseInput()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = characterCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit))
            {
                hit.collider.GetComponent<Handle>()?.Invoke();
            }
        }
    }

    private void UpdatePosition()
    {
        switch (looking)
        {
            case Looking.LEAN_LEFT:
                transform.position = Vector3.Lerp(transform.position, leanLeft.position, mvmtMult * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, leanLeft.rotation, mvmtMult * Time.deltaTime);
                break;
            case Looking.LEAN_RIGHT:
                transform.position = Vector3.Lerp(transform.position, leanRight.position, mvmtMult * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, leanRight.rotation, mvmtMult * Time.deltaTime);
                break;
            case Looking.LOOK_LEFT:
                transform.position = Vector3.Lerp(transform.position, lookLeft.position, mvmtMult * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, lookLeft.rotation, mvmtMult * Time.deltaTime);
                break;
            case Looking.LOOK_RIGHT:
                transform.position = Vector3.Lerp(transform.position, lookRight.position, mvmtMult * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, lookRight.rotation, mvmtMult * Time.deltaTime);
                break;
            case Looking.CENTER:
                transform.position = Vector3.Lerp(transform.position, center.position, mvmtMult*2 * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, center.rotation, mvmtMult*2 * Time.deltaTime);
                break;
            default:
                break;
        }
    }
}
